package com.imooc.vo;

import lombok.*;


@EqualsAndHashCode@ToString
@NoArgsConstructor@AllArgsConstructor
public class User {
    /**  用户名  */
    @Getter@Setter
    private  String username;

    /**  密码  */
    @Getter@Setter
    private  String password;

}
