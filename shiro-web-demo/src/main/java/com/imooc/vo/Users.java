package com.imooc.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.*;

@ToString@EqualsAndHashCode
@NoArgsConstructor@AllArgsConstructor
public class Users {

    /** id */
    @Getter@Setter
    @JSONField(name = "ID")
    private Integer id;

    /** 用户名 */
    @Getter@Setter
    private String username;

    /** 用户密码 */
    @Getter@Setter
    @JSONField(serialize = false)//密码不许序列化
    private String password;

}