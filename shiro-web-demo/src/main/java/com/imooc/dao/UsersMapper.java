package com.imooc.dao;

import com.imooc.vo.Users;

import java.util.List;
import java.util.Set;

public interface UsersMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Users record);

    int insertSelective(Users record);

    Users selectByPrimaryKey(Integer id);

    String getPasswordByName(String username);

    Users selectByUsername(String username);

    Set<String> selectRoleByUsername(String username);

    Set<String> selectPermissionsByUsername(String username);

    int updateByPrimaryKeySelective(Users record);

    int updateByPrimaryKey(Users record);
}