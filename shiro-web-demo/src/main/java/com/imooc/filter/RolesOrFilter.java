package com.imooc.filter;

import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authz.AuthorizationFilter;


/**
 * 自定义filter
 * demo：roles策略：是多个同时具有才可以访问
 * 自定功能：拥有任何一个角色即可访问
 */
public class RolesOrFilter extends AuthorizationFilter {

    @Override
    protected boolean isAccessAllowed(javax.servlet.ServletRequest servletRequest,
                                      javax.servlet.ServletResponse servletResponse,
                                      Object o) throws Exception {
        //先获取主体
        Subject subject = getSubject(servletRequest,servletResponse);

        String[] roles = (String[]) o;
        if(roles == null || roles.length == 0){
            return  true;
        }
        for (String role:roles){
            if(subject.hasRole(role)){
                return  true;
            }
        }
        return false;
    }
}
