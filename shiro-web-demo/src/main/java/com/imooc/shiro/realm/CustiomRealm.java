package com.imooc.shiro.realm;

import com.imooc.dao.UsersMapper;
import com.imooc.vo.Users;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 自定义Realm
 */
public class CustiomRealm extends AuthorizingRealm {
    @Autowired
    private UsersMapper usersMapper;

    /**
     * 授权认证
     * @param principalCollection
     * @return
     */
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        //1. 从主体传过来的认证信息中，获取用户名
        String userName = (String) principalCollection.getPrimaryPrincipal();
        Set<String> roles = getRolesByUserName(userName);
        Set<String> permissions = getPermissionsByUserName(userName);

        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        authorizationInfo.addRoles(roles);
        authorizationInfo.addStringPermissions(permissions);
        return authorizationInfo;
    }

    private Set<String> getRolesByUserName(String userName){
        // 去访问数据库(缓存)返回,此处借助map模拟操作
        Set<String> roles = usersMapper.selectRoleByUsername(userName);
        return roles;
    }

    private Set<String> getPermissionsByUserName(String userName){
        // 去访问数据库(缓存)返回,此处借助map模拟操作
        Set<String> permissions = usersMapper.selectPermissionsByUsername(userName);
        return permissions;
    }
    /**
     * 身份验证
     * @param authenticationToken
     * @return
     * @throws AuthenticationException
     */
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        //1. 从主体传过来的认证信息中，获取用户名
        String userName = (String) authenticationToken.getPrincipal();

        //2. 通过用户名去数据库中获取凭证
        String password = getPasswordByName(userName);
        if(password == null){
            return null;
        }
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(userName,password,"custiomRealm");
        authenticationInfo.setCredentialsSalt(ByteSource.Util.bytes("Mark"));//加盐
        return authenticationInfo;
    }

    private String getPasswordByName(String username){
        String password = usersMapper.getPasswordByName(username);
        return password;
    }

    public static void main(String[] args) {
        Md5Hash md5Hash = new Md5Hash("1234567","Mark");
        System.out.println(md5Hash.toString());
    }

}
