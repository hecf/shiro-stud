package com.imooc.test;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.SimpleAccountRealm;
import org.apache.shiro.subject.Subject;
import org.junit.Before;
import org.junit.Test;

/**
 * Demo2: 判断用户是否拥有角色，借助认证的demo做一个角色判断的demo
 */
public class AuthorizerDemo1 {

    SimpleAccountRealm simpleAccountRealm = new SimpleAccountRealm();//借助一个简单的账户Realm

    @Before
    public void beforeEnv(){
        simpleAccountRealm.addAccount("Mark","123456","admin","user");
    }

    @Test
    public void testAuthentication(){
        //1. 认证第一步：构建Security Manager 的环境
        DefaultSecurityManager defaultSecurityManager = new DefaultSecurityManager();
        defaultSecurityManager.setRealm(simpleAccountRealm);//将Realm设置到环境中。

        //2. 认证第二步：主体提交认证请求；（从SecurityUtils中获取主体）
        SecurityUtils.setSecurityManager(defaultSecurityManager);//使用 SecurityUtils 之前，先设置 Security Manager 环境。
        Subject subject = SecurityUtils.getSubject();
        //构建tokean：认证的时候需要带过去。
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken("Mark","123456");
        subject.login(usernamePasswordToken);//登录

       //判断角色授权
        subject.checkRole("admin");
//        subject.checkRole("admin1");

        subject.checkRoles("admin","user");
//        subject.checkRoles("admin","user1");
        subject.checkRoles("admin1","user1");





    }
}
