package com.imooc.test;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.SimpleAccountRealm;
import org.apache.shiro.subject.Subject;
import org.junit.Before;
import org.junit.Test;

/**
 * Demo1：借助 DefaultSecurityManager 和 SimpleAccountRealm 做一个Shiro认证登入登出的功能
 */
public class AuthenticationTest {

    SimpleAccountRealm simpleAccountRealm = new SimpleAccountRealm();//借助一个简单的账户Realm

    @Before
    public void beforeEnv(){
        simpleAccountRealm.addAccount("Mark","123456");
    }

    @Test
    public void testAuthentication(){
        //1. 认证第一步：构建Security Manager 的环境
        DefaultSecurityManager defaultSecurityManager = new DefaultSecurityManager();
        defaultSecurityManager.setRealm(simpleAccountRealm);//将Realm设置到环境中。

        //2. 认证第二步：主体提交认证请求；（从SecurityUtils中获取主体）
        SecurityUtils.setSecurityManager(defaultSecurityManager);//使用 SecurityUtils 之前，先设置 Security Manager 环境。
        Subject subject = SecurityUtils.getSubject();
        //构建tokean：认证的时候需要带过去。
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken("Mark","123456");
        subject.login(usernamePasswordToken);//登录

        System.out.println("isAuthenticated："+subject.isAuthenticated());//判断是否已经认证成功

        subject.logout();
        System.out.println("isAuthenticated："+subject.isAuthenticated());//判断是否已经认证成功


    }

}
