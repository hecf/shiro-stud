package com.imooc.test;

import com.imooc.shiro.realm.CustiomRealm;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.subject.Subject;
import org.junit.Test;

/**
 * Demo5：自定义Realm的测试
 */
public class CustiomRealmTest {
    @Test
    public void testAuthentication(){
        //1. 认证第一步：构建Security Manager 的环境
        DefaultSecurityManager defaultSecurityManager = new DefaultSecurityManager();
        defaultSecurityManager.setRealm(new CustiomRealm());//将Realm设置到环境中。

        //2. 认证第二步：主体提交认证请求；（从SecurityUtils中获取主体）
        SecurityUtils.setSecurityManager(defaultSecurityManager);//使用 SecurityUtils 之前，先设置 Security Manager 环境。
        Subject subject = SecurityUtils.getSubject();
        //构建tokean：认证的时候需要带过去。
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken("Mark","123456");
        subject.login(usernamePasswordToken);//登录

        subject.checkRole("admin");

        subject.checkPermission("file:delete");

    }

    /**
     * Demo6:
     */
    @Test
    public void testAuthentication2(){
        //1. 认证第一步：构建Security Manager 的环境
        DefaultSecurityManager defaultSecurityManager = new DefaultSecurityManager();
        CustiomRealm custiomRealm = new CustiomRealm();
        //使用加密工具（算法）：
        HashedCredentialsMatcher matcher = new HashedCredentialsMatcher();
        matcher.setHashAlgorithmName("md5");
        matcher.setHashIterations(1);
        custiomRealm.setCredentialsMatcher(matcher);
        defaultSecurityManager.setRealm(custiomRealm);//将Realm设置到环境中。


        //2. 认证第二步：主体提交认证请求；（从SecurityUtils中获取主体）
        SecurityUtils.setSecurityManager(defaultSecurityManager);//使用 SecurityUtils 之前，先设置 Security Manager 环境。
        Subject subject = SecurityUtils.getSubject();
        //构建tokean：认证的时候需要带过去。
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken("Mark2","1234567");
        subject.login(usernamePasswordToken);//登录
    }

}
