package com.imooc.test;

import jdbc.util.DruidJdbcUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.jdbc.JdbcRealm;
import org.apache.shiro.subject.Subject;
import org.junit.Test;

/**
 * Demo4：JDBC Realm的一个测试
 * 注意：JdbcRealm的使用，必须设置权限开关，默认为false
 */
public class JdbcRealmTest {
    @Test
    public void testAuthentication(){
        //1. 认证第一步：构建Security Manager 的环境
        DefaultSecurityManager defaultSecurityManager = new DefaultSecurityManager();
        JdbcRealm jdbcRealm = new JdbcRealm();
        defaultSecurityManager.setRealm(jdbcRealm);//将Realm设置到环境中。
        jdbcRealm.setDataSource(DruidJdbcUtil.dataSource);
        jdbcRealm.setPermissionsLookupEnabled(true);//注意：必须设置权限开关

        //2. 认证第二步：主体提交认证请求；（从SecurityUtils中获取主体）
        SecurityUtils.setSecurityManager(defaultSecurityManager);//使用 SecurityUtils 之前，先设置 Security Manager 环境。
        Subject subject = SecurityUtils.getSubject();
        //构建tokean：认证的时候需要带过去。
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken("Mark","123456");
        subject.login(usernamePasswordToken);//登录

        //判断角色授权
        subject.checkRole("admin");
//        subject.checkRole("admin1");

        subject.checkRoles("admin","user");


        subject.checkPermission("user:delete");


    }
}
